import math, redis, sys, boto.kinesis, Tkinter, pickle, PIL.Image as Image, PIL.ImageTk as ImageTk
from boto.kinesis.layer1 import KinesisConnection

KINESIS_STREAM = "debug_stream"
__author__ = 'maartenm'

class ConsumerApp(Tkinter.Tk):
    def __init__(self, kinesis, resolution, redis):
        Tkinter.Tk.__init__(self)
        self.population = {}
        self.kinesis = kinesis
        self.resolution = resolution
        self.redis = redis
        self.label = Tkinter.Label(text="your image here", compound="top")
        self.label.pack(side="top", padx=8, pady=8)
        self.maxval = 0

        # Get the stream description
        description = kinesis.describe_stream(KINESIS_STREAM)
        shardIds = map(lambda x: x["ShardId"], description['StreamDescription']['Shards'])
        print shardIds[0]
        iterator = kinesis.get_shard_iterator(KINESIS_STREAM, shardIds[0], "TRIM_HORIZON")
        print str(iterator)
        self.update(100, iterator["ShardIterator"])

    def update(self, delay, iterator):
        print "Running update {0}".format(iterator)
        sys.stdout.flush()
        records = self.kinesis.get_records(iterator, 200)
        next_iterator = records["NextShardIterator"]
        for packed_data in records['Records']:
            data = pickle.loads(packed_data['Data'])
            self.add_points(data, 0)
        self.after(delay, self.update, 100, next_iterator)

    def add_points(self, points, generation):
        print "Adding {0} points".format(len(points))
        for point in points:
            x, y = point
            if x<-2 or x>1 or y<-1 or y>1:
                continue
            sx, sy = buddha_to_screen(x, y, self.resolution)
            c = int(self.redis.incr("{0}_{1}".format(sx, sy)))
            print c
            if c > self.maxval:
                self.maxval = c
                self.redis.set("max", self.maxval)

def open_ui():
    return None

def buddha_to_screen(x, y, r):
    return (int((x+2)*r), int((y+1)*r))

if __name__ == "__main__":
    resolution = int(sys.argv[1])
    aws_conf = open("aws.conf")
    access_key = aws_conf.readline().rstrip()
    secret_key = aws_conf.readline().rstrip()

    kinesis = boto.kinesis.connect_to_region('us-east-1',
                                             aws_access_key_id=access_key,
                                             aws_secret_access_key=secret_key)
    assert isinstance(kinesis, KinesisConnection)
    redis = redis.StrictRedis(host="localhost", port=6379, db=3)

    app=ConsumerApp(kinesis, resolution, redis)
    app.redis.flushdb()
    app.mainloop()


