import sys, redis, PIL.Image as Image, math

rdb = redis.StrictRedis(db=0)

resolution = int(sys.argv[1])
image = Image.new('RGB', (resolution * 3, resolution * 2), "black")
pixels = image.load()

els = rdb.keys("*")

#max = int(rdb.get("max"))
def buddha_to_screen(x, y, r):
    return (int((x+2)*r), int((y+1)*r))

for el in els:
    x,y = map(float, rdb.get(el).split("_"))
    sx, sy = buddha_to_screen(x, y, resolution)
    pixels[sx, sy] = (255, 255, 255)
    continue
    val = rdb.get(el)
    r = int(255 * math.sqrt(float(val)/max))
    if x<image.size[1] and y<image.size[0]:
        pixels[y, x] = (r, r, r)

image.show()
