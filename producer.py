import math, sys, redis, boto.kinesis, time, pickle
from boto.kinesis.layer1 import KinesisConnection
__author__ = 'maartenm'

N_POINTS_PER_GEN = 100
N_POINTS_PER_UPLOAD = 1000

KINESIS_STREAM = "debug_stream"

def next_point(points_db, control_db):
    if control_db.get("cursor") is None:
        return None
    next_key = control_db.incr("cursor")
    point = points_db.get(next_key)
    if point is None:
        return None
    return tuple(map(float, point.split("_")))

def process_point((x, y), d):
    points = []
    n = 0
    ox = 0.0
    oy = 0.0

    while n < d:
        nx = ox*ox - oy*oy + x
        ny = 2*ox*oy + y
        dst = nx*nx + ny*ny

        if dst > 4:
            return points

        points.append((nx, ny))

        ox = nx
        oy = ny
        n += 1
    return points

if __name__ == "__main__":
    redis_server = sys.argv[1]
    depth = int(sys.argv[2])
    aws_conf = open("aws.conf")
    access_key = aws_conf.readline().rstrip()
    secret_key = aws_conf.readline().rstrip()

    rdb = redis.StrictRedis(host=redis_server, port=6379, db=0)
    cdb = redis.StrictRedis(host=redis_server, port=6379, db=1)
    kinesis = boto.kinesis.connect_to_region('us-east-1',
                                             aws_access_key_id=access_key,
                                             aws_secret_access_key=secret_key)
    assert isinstance(kinesis, KinesisConnection)
    buffer = []
    while True:
        point = next_point(rdb, cdb)
        if point:
            trail = process_point(point, depth)
            if trail is None:     # point escaped anyway
                continue
            else:
                buffer.extend(trail)
            buffersize = len(buffer)
            if buffersize > N_POINTS_PER_UPLOAD:
                for chunk in (buffer[x:x+N_POINTS_PER_UPLOAD] for x in range(0, len(buffer), N_POINTS_PER_UPLOAD)):
                    print "Uploading {0} points: ".format(len(chunk))
                    response = kinesis.put_record(KINESIS_STREAM, pickle.dumps(chunk), "shard")
                    print response
                buffer = []
        else:
            print "No work for producer"
            sys.stdout.flush()
            time.sleep(1)


