import math, sys, redis, os
__author__ = 'maartenm'


def save_co_mandelbrot(r, depth, db_name):
    rdb = redis.StrictRedis(host='localhost', port=6379, db=0)
    rdb.flushdb()

    rdb.config_set('dbfilename', db_name)
    rdb.config_set('dir', os.path.dirname(os.path.abspath(__file__)))
    n = 0
    for sx in xrange(0, r * 3):
        for sy in xrange(0, r * 2):
            x, y = screen_to_buddha(sx, sy, r)
            if not is_mandelbrot(x, y, depth):
                add_point(n, x, y, rdb)
                n += 1

    rdb.save()

    cdb = redis.StrictRedis(host='localhost', port=6379, db=1)
    cdb.set('cursor', -1)
    pass

def upload_db(dbname):
    pass

def screen_to_buddha(sx, sy, r):
    return float(sx)/r - 2, float(sy)/r - 1

def add_point(n, x, y, rdb):
    rdb.set(n, "{0}_{1}".format(x, y))

def is_mandelbrot(x, y, d):
    a = (x-0.25)*(x-0.25) + y*y
    p = math.sqrt(a)
    # premature exit: the center "holes" of mandelbrot
    if x < p - 2*p*p + 0.25 or (x + 1) * (x + 1) + y * y < 1.0 / 16.0:
        return True
    n = 0
    ox = 0.0
    oy = 0.0
    while n < d:
        nx = ox*ox - oy*oy + x
        ny = 2*ox*oy + y
        dst = nx*nx + ny*ny

        if dst >= 4.0:
            return False    # point escaped

        ox = nx
        oy = ny
        n += 1
    return True

if __name__ == "__main__":
    resolution = int(sys.argv[1])
    depth = int(sys.argv[2])
    db_name = "{0}_{1}.db".format(resolution, depth)
    save_co_mandelbrot(resolution, depth, db_name)
    upload_db(db_name)

